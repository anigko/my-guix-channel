;;; alps-core: ALPSCore library package definition for GNU Guix
;;; Copyright (C) 2024 Nigko Yerden <nigko.yerden@gmail.com>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (alps-core)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system cmake)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix build utils)
  #:use-module (guix utils)
  #:use-module (guix gexp)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages graphviz))

(define-public alps-core
  (package
    (name "alps-core")
    (version "2.3.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://github.com/ALPSCore/ALPSCore/archive/refs/tags/v"
             version ".tar.gz"))
       (sha256
        (base32 "0v6q5v8wsjj9bw4ahg7q3d7d542xgawqs8z8kz4imv9xak6jakrq"))))
    (build-system cmake-build-system)
    (arguments
     (list
      #:phases #~(modify-phases %standard-phases
                   (add-after 'patch-source-shebangs 'sh-in-tests
                     (lambda _
                       (substitute* "common/cmake/ALPSEnableTests.cmake"
                         (("/bin/sh") (string-append #$bash "/bin/sh")))))
                   (add-after 'sh-in-tests 'fix-doxygen
                     (lambda _
                       (substitute* "common/doc/Doxyfile.in"
                         (("^GENERATE_LATEX.*") "GENERATE_LATEX = NO"))))
                   (add-before 'build 'make-cache-dir-for-fontconfig
                     (lambda _
                       (mkdir ".cache")
                       (chmod ".cache" #o775)
                       (setenv "XDG_CACHE_HOME" ".cache")))
                   (add-after 'build 'set-mpi-env-for-tests
                     (lambda _
                       (setenv "OMPI_MCA_plm_rsh_agent" "")))
                   (add-after 'install 'move-doc
                     (lambda* (#:key outputs #:allow-other-keys)
                       (let ((out (assoc-ref outputs "out"))
                             (doc (assoc-ref outputs "doc")))
                         (mkdir-p (string-append doc "/share/doc"))
                         (rename-file
                          (string-append out "/share/doc/ALPSCore")
                          (string-append doc "/share/doc/ALPSCore"))))))))
    (inputs (list boost hdf5 eigen openmpi doxygen graphviz))
    (outputs '("out" "doc" "debug"))
    (synopsis "Applications and Libraries for Physics Simulations Core
libraries")
    (description "The ALPSCore project, based on the ALPS (Algorithms and
Libraries for Physics Simulations) project, provides generic algorithms
and utilities for physics problems. It strives to increase software reuse
in the physics community.")
    (home-page "https://alpscore.org/")
    (license license:gpl2+)))

alps-core
