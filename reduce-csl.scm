;;; reduce-csl: REDUCE computer algebra package definition for GNU Guix
;;; Copyright (C) 2023 Nigko Yerden <nigko.yerden@gmail.com>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (reduce-csl)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages base)
  #:use-module (gnu packages autotools))

(define-public reduce-csl
  (package
    (name "reduce-csl")
    (version "6860")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "mirror://sourceforge/reduce-algebra/snapshot_"
                    "2024-08-12" "/Reduce-svn" version "-src.tar.gz"))
              (sha256
               (base32
                "13bij9d4dj96vd5di59skz77s2fihj7awmkx403fvh9rd04ly25z"))
              (modules '((guix build utils)))
              ;; remove binaries and unnecessary parts
              ;; to ensure we build from source files only
              (snippet '(map delete-file-recursively
                         (append (find-files "csl/generated-c" "\\.img$")
                          '("psl" "vsl"
                            "jlisp"
                            "jslisp"
                            "libedit"
                            "macbuild"
                            "MacPorts"
                            "mac-universal"
                            "reduce2"
                            "winbuild64"
                            "common-lisp"
                            "contrib"
                            "generic/qreduce"
                            "web/htdocs/images/Thumbs.db")
                          (find-files "csl"
                           "^(embedded|new-embedded|winbuild|support-packages)$"
                           #:directories? #t)
                          (find-files "libraries"
                           "^(original|wineditline|libffi-for-mac)$"
                           #:directories? #t))))))
    (build-system gnu-build-system)
    (arguments
     (list #:configure-flags
           #~(list "--without-autogen"
                   ;; fix conflict with internal build name determination
                   "--build="
                   "--with-csl"
                   (string-append "CPPFLAGS=-I"
                                  #$freetype
                                  "/include/freetype2"))
           #:make-flags #~(list "csl")
           #:phases
           #~(modify-phases %standard-phases
               (replace 'check
                 (lambda* (#:key tests? #:allow-other-keys)
                   (when tests?
                     (invoke "scripts/testall.sh" "--csl" "--noregressions"))))
               (add-before 'patch-source-shebangs 'autogen
                 (lambda _
                   (invoke "sh" "autogen.sh")))
               (add-after 'install 'fix-install
                 (lambda _
                   (copy-file "bin/rfcsl"
                              (string-append #$output "/bin/rfcsl"))
                   (copy-file "generic/newfront/redfront.1"
                              (string-append #$output
                                             "/share/man/man1/rfcsl.1"))
                   (let ((.desktop-file
                          "debianbuild/reduce/debian/redcsl.desktop")
                         (icon "debianbuild/reduce/debian/reduce.png"))
                     (install-file .desktop-file
                                   (string-append #$output
                                                  "/share/applications"))
                     (install-file icon
                                   (string-append
                                    #$output
                                    "/share/icons/hicolor/32x32/apps")))
                   (with-directory-excursion #$output
                     (map (lambda (dir)
                            (map (lambda (file)
                                   (chmod file #o444))
                                 (find-files dir)))
                          '("share/man/man1" "share/reduce/fonts"))))))))
    (native-inputs (list which autoconf automake libtool))
    (inputs
     ;; bundled libraries: fox (adjusted) editline (adjusted)
     ;; libffi crlibm softfloat
     (list ncurses freetype libxft libx11 libxext))
    (synopsis "Portable general-purpose computer algebra system")
    (description
     "REDUCE is a portable general-purpose computer algebra system.  It is a
system for doing scalar, vector and matrix algebra by computer, which also
supports arbitrary precision numerical approximation and interfaces to gnuplot
to provide graphics.  It can be used interactively for simple calculations but
also provides a full programming language, with a syntax similar to other
modern programming languages.  REDUCE supports alternative user interfaces
including Run-REDUCE, TeXmacs and GNU Emacs.  This package provides Codemist
Standard Lisp (CSL) version of REDUCE.  It uses gnuplot program, if installed,
to make figures.")
    (home-page "https://reduce-algebra.sourceforge.io/")
    (license (license:non-copyleft "file://README"
                                   "See README in the deistribution."))))

reduce-csl
