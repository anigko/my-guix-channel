;;; boost: C++ Boost library package definitions for GNU Guix
;;; Copyright (C) 2023-2024 Nigko Yerden <nigko.yerden@gmail.com>
;;; Most of the following is taken from gnu/packages/boost.scm
;;; from GNU Guix repository https://git.savannah.gnu.org/cgit/guix.git
;;; at commit ba07aad5b33db0cbaa862ffca7f85de26e44b6af. See the full
;;; list of authors there.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (boost-custom)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix utils)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages icu4c)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages mpi))

(define (version-with-underscores version)
  (string-map (lambda (x) (if (eq? x #\.) #\_ x)) version))

(define (boost-patch name version hash)
  (origin
    (method url-fetch)
    (uri (string-append "https://www.boost.org/patches/"
                        (version-with-underscores version) "/" name))
    (file-name (string-append "boost-" name))
    (sha256 (base32 hash))))

(define-public boost-1.8
  (package
    (name "boost")
    (version "1.80.0")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://boostorg.jfrog.io/artifactory/main/release/"
                                  version "/source/boost_"
                                  (version-with-underscores version) ".tar.bz2"))
              (patches
               (list (boost-patch
                      ;; Boost.Filesystem directory iterators may fail to
                      ;; construct for a network share on Windows prior to 10:
                      ;; https://github.com/boostorg/filesystem/pull/246,
                      ;; https://github.com/boostorg/filesystem/issues/245
                      "0001-filesystem-win-fix-dir-it-net-share.patch" version
                      "067hhylqkzzdbqzc1hkbpaqmvz248lxqrdhb2yi6iq9qabsik3lk")
                     (boost-patch
                      ;; In Boost.Filesystem on Windows, weakly_canonical fails
                      ;; to process paths that start with the "\\?\" prefix:
                      ;; https://github.com/boostorg/filesystem/issues/247
                      "0002-filesystem-fix-weakly-canonical-long-paths.patch" version
                      "00w3albf8527glclx85p5b2ml3vr06xpwwmfyzg005v1cp8avcpi")
                     (boost-patch
                      ;; Boost.Unordered containers are not in a valid state
                      ;; after moving:
                      ;; https://github.com/boostorg/unordered/issues/139
                      "0003-unordered-valid-after-move.patch" version
                      "0dw839w22cawqawfpsx7j7v9y0x2vn66m732iidpxvdxbjn2kzva")
                     (boost-patch
                      ;; Fixed a missing include on POSIX systems that don't
                      ;; support *at APIs:
                      ;; https://github.com/boostorg/filesystem/issues/250
                      "0004-filesystem-posix-fix-no-at-apis-missing-include.patch" version
                      "09k8k3b1306jkjls12wfghj820n828j6aaxzmcr0wpnjhp8fzi1v")))
              (sha256
               (base32
                "1h00qp4z5k6lfz310xjwsmqs8fwxi6ngas51169cafz4h9fmc68y"))))
    (build-system gnu-build-system)
    (inputs
     (append
      (list icu4c zlib)
      (if (%current-target-system)
          '()
          (list python-minimal-wrapper))))
    (native-inputs
     (list perl tcsh))
    (arguments
     (list
      #:imported-modules `((guix build python-build-system)
                           ,@%default-gnu-imported-modules)
      #:modules `(((guix build python-build-system) #:select (python-version))
                  ,@%default-gnu-modules)
      #:tests? #f
      #:configure-flags
      #~(let ((icu (dirname (dirname (search-input-file
                                      %build-inputs "bin/uconv")))))
          (list
           ;; Auto-detection looks for ICU only in traditional
           ;; install locations.
           (string-append "--with-icu=" icu)
           ;; Ditto for Python.
           #$@(if (%current-target-system)
                  #~()
                  #~((let ((python (dirname (dirname (search-input-file
                                                      %build-inputs
                                                      "bin/python")))))
                       (string-append "--with-python-root=" python)
                       (string-append "--with-python=" python
                                      "/bin/python")
                       (string-append "--with-python-version="
                                      (python-version python)))))
           "--with-toolset=gcc"))
      #:make-flags
      #~(list "threading=multi" "link=shared"

              ;; Set the RUNPATH to $libdir so that the libs find each other.
              (string-append "linkflags=-Wl,-rpath="
                             #$output "/lib")
              #$@(if (%current-target-system)
                     #~("--user-config=user-config.jam"
                        ;; Python is not supported when cross-compiling.
                        "--without-python"
                        "binary-format=elf"
                        "target-os=linux"
                        #$@(cond
                            ((string-prefix? "arm" (%current-target-system))
                             #~("abi=aapcs"
                                "address-model=32"
                                "architecture=arm"))
                            ((string-prefix? "aarch64" (%current-target-system))
                             #~("abi=aapcs"
                                "address-model=64"
                                "architecture=arm"))
                            (else #~())))
                     #~()))
      #:phases
      #~(modify-phases %standard-phases
          (add-after 'unpack 'patch-shells
            (lambda _
              (substitute* '("libs/config/configure"
                             "libs/spirit/classic/phoenix/test/runtest.sh"
                             "tools/build/src/engine/execunix.cpp")
                (("/bin/sh") (which "sh")))))
          (delete 'bootstrap)
          (replace 'configure
            (lambda* (#:key (configure-flags ''()) #:allow-other-keys)
              (setenv "SHELL" (which "sh"))
              (setenv "CONFIG_SHELL" (which "sh"))

              #$@(if (%current-target-system)
                     #~((call-with-output-file "user-config.jam"
                          (lambda (port)
                            (format port
                                    "using gcc : cross : ~a-c++ ;"
                                    #$(%current-target-system)))))
                     #~())

              (apply invoke "./bootstrap.sh"
                     (string-append "--prefix=" #$output)
                     configure-flags)))
          (replace 'build
            (lambda* (#:key make-flags #:allow-other-keys)
              (apply invoke "./b2"
                     (format #f "-j~a" (parallel-job-count))
                     make-flags)))
          (replace 'install
            (lambda* (#:key make-flags #:allow-other-keys)
              (apply invoke "./b2" "install" make-flags)))
          #$@(if (%current-target-system)
                 #~()
                 #~((add-after 'install 'provide-libboost_python
                      (lambda* (#:key make-flags inputs outputs #:allow-other-keys)
                        (let* ((static? (member "link=static" make-flags))
                               (libext (if static? ".a" ".so"))
                               (python (dirname (dirname (search-input-file
                                                          inputs "bin/python"))))
                               (python-version (python-version python))
                               (libboost_pythonNN
                                (string-append "libboost_python"
                                               (string-join (string-split
                                                             python-version #\.)
                                                            "")
                                               libext)))
                          (with-directory-excursion (string-append #$output "/lib")
                            (symlink libboost_pythonNN
                                     (string-append "libboost_python" libext))
                            ;; Some packages only look for the major version.
                            (symlink libboost_pythonNN
                                     (string-append "libboost_python"
                                                    (string-take python-version 1)
                                                    libext)))))))))))

    (home-page "https://www.boost.org")
    (synopsis "Peer-reviewed portable C++ source libraries")
    (description
     "A collection of libraries intended to be widely useful, and usable
across a broad spectrum of applications.")
    (license (license:x11-style "https://www.boost.org/LICENSE_1_0.txt"
                                "Some components have other similar licences."))))

(define-public boost-1.8-mpi
  (package
    (inherit boost-1.8)
    (name "boost-mpi")
    (inputs
     (modify-inputs (package-inputs boost-1.8)
       (append openmpi)))
    (arguments
     (substitute-keyword-arguments (package-arguments boost-1.8)
       ((#:phases phases)
        #~(modify-phases #$phases
            (add-after 'configure 'update-jam
              (lambda* (#:key inputs outputs #:allow-other-keys)
                (let ((output-port (open-file "project-config.jam" "a")))
                  (display "using mpi ;" output-port)
                  (newline output-port)
                  (close output-port))))))))
    (home-page "https://www.boost.org")
    (synopsis "Message Passing Interface (MPI) library for C++")))

(define-public boost-1.8-mpi-numpy
  (package
    (inherit boost-1.8-mpi)
    (name "boost-mpi-numpy")
    (inputs
     (modify-inputs (package-inputs boost-1.8-mpi)
       (append python-numpy)))
    (synopsis "Boost library with mpi and numpy sublibraries")))

(define-public boost-mpi-numpy
  (package
    (inherit boost-mpi)
    (name "boost-mpi-numpy")
    (inputs
     (modify-inputs (package-inputs boost-mpi)
       (append python-numpy)))
    (synopsis "Boost library with mpi and numpy sublibraries")))
