;;; alps: ALPS library package definition for GNU Guix
;;; Copyright (C) 2023-2024 Nigko Yerden <nigko.yerden@gmail.com>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (alps)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system cmake)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix utils)
  #:use-module (guix gexp)
  #:use-module (boost-custom)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages llvm)    ;; for libomp
  #:use-module (gnu packages algebra) ;; for fftw
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages compression) ;; zlib
  #:use-module (gnu packages xml)	  ;; expat
  #:use-module (gnu packages commencement) ;; gfortran-toolchain
  )

(define license (@@ (guix licenses) license))

(define* (nonfree uri #:optional (comment ""))
  "Return a nonfree license, whose full text can be found
at URI, which may be a file:// URI pointing the package's tree."
  (license "Nonfree"
           uri
           (string-append
            "This a nonfree license.  Check the URI for details.  "
            comment)))

(define-public alps
  ;; Almost all the library except 'tebd' application builds well with
  ;; more recent versions of hdf5 library, e.g. ver. 1.14. Turn off
  ;; ALPS-BUILD-FORTRAN cmake switch to exclude 'tebd' from build.
  (let ((hdf5-pkg hdf5-1.8))
    (package
      (name "alps")
      (version "7871")
      (source
       (origin
         (method url-fetch)
         (uri (string-append
               "http://exa.phys.s.u-tokyo.ac.jp/archive/MateriApps/src/"
               "alps_20220304~r7871.orig.tar.gz"))
         (file-name (string-append name "-" version ".tar.gz"))
         (sha256
          (base32 "09klx0samxz9lap2j9q19264bdpydbrfqq1wmwwi9y01cwavkg3l"))))
      (build-system cmake-build-system)
      (arguments
       (let ((python-version (version-major+minor (package-version python))))
         (list
          #:out-of-source? #t
          #:configure-flags #~(list
                               "-DALPS_ENABLE_OPENMP=ON"
                               "-DALPS_ENABLE_OPENMP_WORKER=ON"
                               "-DALPS_BUILD_PYTHON=ON"
                               (string-append
                                "-DPYTHON_LIBRARY="
                                #$python
                                "/lib/libpython"
                                #$python-version
                                ".so")
                               "-DALPS_BUILD_FORTRAN=ON"
                               (string-append
                                "-DCMAKE_Fortran_FLAGS=-I"
                                #$hdf5-pkg:fortran
                                "/include"))
          #:phases #~(modify-phases %standard-phases
                       (add-after 'unpack 'fix-python-collections
                         (lambda _
                           (map (lambda (file)
                                  (substitute*
                                      file
                                    (("^from collections import")
                                     "from collections.abc import")))
                                (find-files ".." "\\.py$"))))
                       (add-after 'fix-python-collections 'fix-timer
                         (lambda _
                           (map (lambda (file)
                                  (substitute*
                                      file
                                    (("^#include <boost/timer.hpp>" all)
                                     (format #f "
#ifndef BOOST_TIMER_ENABLE_DEPRECATED
#define BOOST_TIMER_ENABLE_DEPRECATED
#endif
~a~%" all))))
                                (find-files ".." "\\.(C|cpp)$"))))
                       (add-after 'install 'fix-python-paths
                         (lambda _
                           (let* ((libdir (string-append #$output
                                                         "/lib/"))
                                  (site-dir (string-append
                                             libdir
                                             "python"
                                             #$python-version
                                             "/site-packages/")))
                             (mkdir-p site-dir)
                             (map (lambda (file)
                                    (switch-symlinks
                                     (string-append
                                      site-dir
                                      file)
                                     (string-append
                                      libdir file)))
                                  (list
                                   "libalps.so"
                                   "libmps_models.so"
                                   "libmps_utils.so"
                                   "pyalps")))))))))
      (native-inputs (list python-toolchain gfortran-toolchain pkg-config
                           zlib expat))
      (inputs (list boost-mpi-numpy
                    openmpi
                    hdf5-pkg
                    `(,hdf5-pkg "fortran")
                    openblas
                    libomp
                    fftw
                    python-h5py
                    python-numpy
                    python-matplotlib
                    python-scipy))
      (outputs '("out" "debug"))
      (synopsis "Algorithms and Libraries for Physics Simulations")
      (description
       "The ALPS project (Algorithms and Libraries for Physics
Simulations) is an open source effort aiming at providing high-end
simulation codes for strongly correlated quantum mechanical systems as
well as C++ libraries for simplifying the development of such
code.  ALPS strives to increase software reuse in the physics
community.")
      (home-page (string-append "https://web.archive.org/web/20211203054239/"
                                "https://alps.comp-phys.org/mediawiki/index.php/Main_Page"))
      (license (nonfree "file://LICENSE.txt"
                "Note that ALPS applications are distributed under the
separate license, see LICENSE-applications.txt in the deistribution.")))))

alps
